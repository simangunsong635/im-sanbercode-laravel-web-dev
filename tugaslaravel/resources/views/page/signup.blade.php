<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label> First Name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label> Last Name:</label><br><br>
        <input type="text" name="lname"><br><br>
        <label> Gender: </label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label > Nationality:</label><br><br>
        <select name="nationality"><br><br>
            <option value="indonesia">Indonesian</option>
            <option value="english">English</option>
            <option value="other">Other</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="english">English <br>
        <input type="checkbox" name="other">Other <br><br>
        <label >Bio:</label><br><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br><br>
        <input type="submit" value="SignUp">
    </form>
</body>
</html>