<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <!-- Styles -->
    </head>
    <body>
        <h1> Sanberbook</h1>
        <h2> Social Media Developer Santai Berkualitas</h2>
        <p> Belajar dan berbagai agar hidup ini semakin santai dan berkualitas</p>
        <h2> Benefit Join Sanberbook</h1>
        <ul>
            <li>Mendapatkan motivasi dari sesama Developer</li>
            <li>Sharing Knowledge dari para suhu sanber</li>
            <li>Dibuat oleh calon web developer terbaik </li>
        </ul>
        <h2>Cara Bergabung ke Sanberbook</h2>
        <ul>
            <li>Mengunjungi website ini </li>
            <li>Mendaftar Di<a href="/signup"> Form Sign Up</a></li>
            <li>Selesai</li>
        </ul>
    </body>
</html>
