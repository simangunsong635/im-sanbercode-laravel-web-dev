<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//alamat URL
Route::get('/', [HomeController::class,'home']);
Route::get('/signup', [AuthController::class,'daftar']);
Route::post('/welcome', [AuthController::class,'dashboard']);
Route::get('/datatable', function()
{
    return view('page.datatable');
});