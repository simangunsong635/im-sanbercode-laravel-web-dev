<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar ()
    {
        return view ('page.signup');
    }

    public function dashboard(Request $request)
    {
        $firstName = $request->input ('fname');
        $lastName = $request->input ('lname');
        
        return view('page.welcome', ["firstName" =>$firstName, "lastName"=>$lastName]);
    }
}
