<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $animal1 = new animal("Shaun");
    echo "Nama Hewan : " . $animal1->jenis . "<br>"; // "shaun"
    echo "Legs : " . $animal1->legs . "<br>"; // 4
    echo "Cold Blooded : " . $animal1->cold_blooded ."<br><br>"; // "no"

    $sungokong = new ape("Kera Sakti");
    echo "Nama Hewan : " . $sungokong->jenis . "<br>"; 
    echo "Legs : " . $sungokong->legs . "<br>"; 
    echo "Cold Blooded : " . $sungokong->cold_blooded ."<br>";
    echo $sungokong -> suara("Auoo") . "<br><br>";

    $kodok = new frog("buduk");
    echo "Nama Hewan : " . $kodok->jenis . "<br>"; 
    echo "Legs : " . $kodok->legs . "<br>"; 
    echo "Cold Blooded : " . $kodok->cold_blooded ."<br>";
    echo $kodok -> jump("Hop Hop    ") . "<br><br>";

?> 