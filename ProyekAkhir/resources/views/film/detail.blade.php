@extends('layouts.master')
@section('judul')
    Halaman Detail Film
@endsection
@section('content')

<img src="{{asset('poster/'.$film->poster)}}" width="50%" height= "500px" alt="Gambar Tidak ada">
<h1 class="text-primary">{{$film->title}}</h1>
<p>{{$film->content}}</p>
<a href="/film" class="btn btn-secondary btn-sm">Kembali</a>


@endsection