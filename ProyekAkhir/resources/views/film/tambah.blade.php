@extends('layouts.master')
@section('judul')
    Halaman Tambah Film
@endsection
@section('content')
    <form action='/film' method="POST" enctype="multipart/form-data"> 
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @csrf
    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="form-control" >
            <option value="">--Pilih Genre</option>
            @forelse ($genres as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @empty
                <option value="">Tidak Ada Kategori</option>
            @endforelse
        </select>
    </div>
    <div class="form-group">
        <label>Title</label>
        <input type="text" name="title" class="form-control">
    </div>
    <div class="form-group">
        <label>Content</label>
        <textarea name="content" id="" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <label>Year</label>
        <input type="text" name="year" class="form-control">
    </div>
    <div class="form-group">
        <label>Image</label>
        <input type="file" name="poster" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/film" class="btn btn-primary">Kembali</a>
    </form>
@endsection