@extends('layouts.master')
@section('judul')
    Halaman Edit Film
@endsection
@section('content')
    <form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @method("PUT")
    @csrf
    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="form-control" id="" >
            <option value="">--Pilih Genre--</option>
            @forelse ($genres as $item)
                @if ($item->id === $film->id)
                    <option value={{$item->id}} selected>{{$item->name}}</option>
                @else
                    <option value={{$item->id}}>{{$item->name}}</option>
                @endif

            @empty
                <option value="">Tidak Ada Kategori</option>
            @endforelse
        </select>
    </div>
    <div class="form-group">
        <label>Title</label>
        <input type="text" value="{{$film->title}}" name="title" class="form-control">
    </div>
    <div class="form-group">
        <label>Content</label>
        <textarea name="content" id="" class="form-control">{{$film->content}}</textarea>
    </div>
    <div class="form-group">
        <label>Year</label>
        <input type="text" name="year" value="{{$film->year}}" class="form-control">
    </div>
    <div class="form-group">
        <label>Image</label>
        <input type="file" name="poster" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/film" class="btn btn-primary">Kembali</a>
    </form>
@endsection