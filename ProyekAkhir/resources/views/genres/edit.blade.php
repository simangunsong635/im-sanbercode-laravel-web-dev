@extends('layouts.master')
@section('judul')
    Halaman Edit Genre
@endsection
@section('halaman')
    Genre / Edit
@endsection
@section('content')
    <form action="/genres/{{$genre->id}}" method="POST">
    @method('put')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @csrf
    <div class="form-group">
        <label> Nama Genre</label>
        <input type="text" value="{{$genre->name}}" name="name" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection