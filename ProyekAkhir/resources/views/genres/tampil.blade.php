@extends('layouts.master')
@section('judul')
    Halaman Tampil Genre
@endsection
@section('content')

<a href="/genres/create" class="btn btn-primary btn-sm"> Tambah</a>
<table class="table">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($genres as $key => $item)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->name}}</td>
            <td>
                <form action="/genres/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/genres/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/genres/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td>Tidak Ada Genre</td>
        </tr>
        @endforelse
    </tbody>
</table>

@endsection