<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Genre extends Model
{
    use HasFactory;
    protected $table ='genre';
    protected $fillable =['name'];

    public function listFilm(): Hasmany
    {
        return $this ->hasMany(Film::class,'genre_id');
    }
}
