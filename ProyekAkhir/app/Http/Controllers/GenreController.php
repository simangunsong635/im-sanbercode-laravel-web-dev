<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Genre;

class GenreController extends Controller
{
    public function create(){
        return view('genres.tambah');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|min:3'
        ]);

        DB::table('genre')->insert([
            'name' => $request->input('name')
        ]);
        return redirect('/genres');
    }

    public function index(){
        $genres = DB::table('genre')->get();
        return view('genres.tampil', ['genres'=> $genres]);
    }

    public function show($id){
        $genre = Genre::find($id);
        return view('genres.detail',['genre' =>$genre]);
    }

    public function edit($id){
        $genre = DB::table('genre')->find($id);
        return view('genres.edit',['genre' =>$genre]);
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required|min:3'
        ]);
        DB::table('genre')
        ->where('id', $id)
        ->update(
            [
                'name' => $request ->input('name')
            ]
            );
        return redirect('/genres');
    }

    public function destroy($id){
        DB::table('genre')-> where('id', '=', $id) ->delete();

        return redirect('/genres');
    }
}