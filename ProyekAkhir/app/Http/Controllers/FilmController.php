<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Genre;
use File;


class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth') ->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $film = Film::all();
        return view('film.tampil', ['film' => $film]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $genres = Genre::all();
        return view('film.tambah',['genres' => $genres]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request ->validate([
            'title' => 'required|min:3',
            'content' => 'required',
            'year'=>'required',
            'genre_id'=>'required',
            'poster'=>'required|mimes:png,jpg,jpeg|max:2048'
        ]);

        $imageName = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('poster'),$imageName);

        Film::create([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'year' => $request->input('year'),
            'genre_id' => $request->input('genre_id'),
            'poster' =>$imageName,
        ]);

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $film = Film::find($id);
        return view('film.detail',["film" =>$film]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $film = Film::find($id);
        $genres = Genre::all();

        return view('film.edit', ['film' => $film, "genres"=>$genres]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request ->validate([
            'title' => 'required|min:3',
            'content' => 'required',
            'year'=>'required',
            'genre_id'=>'required',
            'poster'=>'mimes:png,jpg,jpeg|max:2048'
        ]);

        $film = Film::find($id);
        if($request ->has ('image')){
            file::delete('image/'.$film->poster);
            $imageName = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('poster'),$imageName);
            $film->image =$imageName;
        }

        $film->title = $request ->input('title');
        $film -> content= $request ->input('content');
        $film -> year= $request ->input('year');
        $film -> genre_id= $request ->input('genre_id');

        return redirect ('/film');

        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $films = Film::find($id);
        File::delete('poster/'.$films->poster);


        $films->delete();
        return redirect('/film');

    }
}
