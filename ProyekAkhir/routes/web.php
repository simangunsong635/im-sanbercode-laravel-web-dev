<?php

use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\DashboardController;
Use App\Http\Controllers\GenreController;
Use App\Http\Controllers\FilmController; 



Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function () {

        // CRUD Genre
        // CRUD Data Genre
        Route::get('/genres/create',[GenreController::class,'create']); //Menghubungkan ke halaman tambah
        Route::post('/genres',[GenreController::class,'store']); //menyimpan data ke database
        Route::get('/genres',[GenreController::class,'store']); //Mengembalikan ke halaman sebelumnya

        // Read data genres
        Route::get('/genres', [GenreController::class,'index']); // untuk menampilkan semua genre ke tabel
        Route::get('/genres/{id}',[GenreController::class,'show']); //untuk menampilkan detail dari genre

        // Update data genres
        Route::get('/genres/{id}/edit', [GenreController::class,'edit']); //mengarahkan ke form edit data
        Route::put('/genres/{id}', [GenreController::class, 'update']); //untuk update data pada database

        // Hapus Data Genres
        Route::delete('/genres/{id}', [GenreController::class,'destroy']);//hapus data di database
         });

        // Membuat CRUD NEWS dengan metode ORM
        Route::resource('film', FilmController::class);




Auth::routes();

