<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\News;
use File;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $news=News::all();
        return view('news.tampil', ['news' =>$news]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::all();
        return view('news.tambah', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request -> validate([
            'title' => 'required|min:3',
            'content' => 'required',
            'category_id'=>'required',
            'image'=>'required|mimes:png,jpg,jpeg|max:2048'
        ]);
        
        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('image'), $imageName);

        News::create([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'category_id'=> $request ->input('category_id'),
            'image' => $imageName,
        ]);

        return redirect('/news');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $news = News::find($id);
        return view('news.detail',["news"=>$news]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $news = News::find($id);
        $categories = Category::all();

        return view('news.edit',["news"=>$news, "categories" =>$categories]);

        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request -> validate([
            'title' => 'required|min:3',
            'content' => 'required',
            'category_id'=>'required',
            'image'=>'mimes:png,jpg,jpeg|max:2048'
        ]);

        $news= News::find($id);
        if($request->has('image')){
            File::delete('image/'.$news->image);
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('image'), $imageName);
            $news->image= $imageName;
            
        }

        $news->title = $request->input('title');
        $news->content= $request->input('content');
        $news->category_id= $request->input('category_id');
        $news->save();

        return redirect('/news');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $news = News::find($id);
        File::delete('image/'.$news->image);


        $news->delete();
        return redirect('/news');

    }
}
