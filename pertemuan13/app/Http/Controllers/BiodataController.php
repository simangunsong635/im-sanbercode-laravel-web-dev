<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function daftar()
    {
        return view('page.biodata');
    }
    
    public function dashboard2(Request $request)
    {
        $firstName = $request->input ('fname');
        $lastName = $request->input ('lname');
        
        return view('page.dashboard', ["firstName" =>$firstName, "lastName"=>$lastName]);
    }
}
