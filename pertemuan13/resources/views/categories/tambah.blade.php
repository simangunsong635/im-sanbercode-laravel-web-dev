@extends('layouts.master')
@section('judul')
    Halaman Tambah
@endsection
@section('content')
    <form action='/category' method="POST">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @csrf
    <div class="form-group">
        <label>Category Name</label>
        <input type="text" name='name' class="form-control">
    </div>
    <div class="form-group">
        <label>Category Description</label>
        <textarea name="description" id="" class="form-control"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection