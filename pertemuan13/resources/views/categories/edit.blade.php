@extends('layouts.master')
@section('judul')
    Halaman Edit Kategori
@endsection
@section('content')
    <form action='/category/{{$category->id}}' method="POST">
    @method('put')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @csrf
    <div class="form-group">
        <label>Category Name</label>
        <input type="text" value="{{$category->name}}" name='name' class="form-control">
    </div>
    <div class="form-group">
        <label>Category Description</label>
        <textarea name="description" id="" class="form-control">{{$category->description}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection