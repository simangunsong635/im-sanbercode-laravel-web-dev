@extends('layouts.master')
@section('judul')
    Halaman Tampil Berita
@endsection
@section('content')
    <a href="/news/create" class="btn btn-primary btn-sm my-3"> Tambah</a>
<div class="row">
    @forelse ($news as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('image/'.$item->image)}}" height="200px" class="card-img-top" alt="/">
            <div class="card-body">
              <h2>{{$item->title}}</h2>
              <p class="card-text">{{Str::limit($item->content, 70)}}</p>
              <a href="/news/{{$item->id}}" class="btn btn-primary btn-block ">Detail</a>
              <div class="row my-3">
                <div class="col">
                    <a href="/news/{{$item->id}}/edit" class="btn btn-warning btn-block ">Edit</a>
                </div>
                <div class="col">
                    <form action="/news/{{$item->id}}" method="POST">
                    @csrf
                    @method("DELETE")
                    <input type="submit" value="Delete" class="btn btn-danger btn-block">
                    </form>
                </div>
              </div>
            </div>
          </div>
    </div>        
    @empty
        <h4>Tidak Ada Berita</h4>
    @endforelse
</div>
@endsection