@extends('layouts.master')
@section('judul')
    Halaman Detail Berita
@endsection
@section('content')

<img src="{{asset('image/'.$news->image)}}" width="100%" height= "300px" alt="Gambar Tidak ada">
<h1 class="text-primary">{{$news->title}}</h1>
<p>{{$news->content}}</p>
<a href="/news" class="btn btn-secondary btn-sm">Kembali</a>


@endsection