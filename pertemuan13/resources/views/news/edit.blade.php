@extends('layouts.master')
@section('judul')
    Halaman Edit Berita
@endsection
@section('content')
    <form action='/news/{{$news->id}}' method="POST" enctype="multipart/form-data">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @method("PUT")
    @csrf
    <div class="form-group">
        <label>Category</label>
        <select name="category_id" class="form-control" id="">
            <option value="">--Pilih Kategori--</option>
            @forelse ($categories as $item)
                @if ($item->id === $news->category_id)
                    <option value={{$item->id}}     selected>{{$item->title}}</option>
                @else
                <option value={{$item->id}}>{{$item->title}}</option>
                    
                @endif

            @empty
            <option value="">Tidak Ada Kategori</option>
            @endforelse
        </select>
    </div>    
    <div class="form-group">
        <label>Title</label>
        <input type="text" value="{{$news->title}}" name='title' class="form-control">
    </div>
    <div class="form-group">
        <label>Content</label>
        <textarea name="content" class="form-control">{{$news->content}}</textarea>
    </div>
    <div class="form-group">
        <label>Image</label>
        <input type="file" name='image'class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/news" class="btn btn-primary btn-sm">Kembali</a>
    </form>
@endsection