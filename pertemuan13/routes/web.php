<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\NewsController;



Route::get('/', function () {
    return view('welcome');
});

//alamat URL
Route::get('/', [DashboardController::class,'home']);
Route::get('/biodata', [BiodataController::class,'daftar']);
Route::post('/dashboard',[BiodataController::class,'dashboard2']);

Route::get('/table', function()
{
    return view('page.table');
});

Route::get('/data-tables', function()
{
    return view('page.data-tables');
});

// CRUD Categories
// Create Data Categories
Route::get('/category/create',[CategoriesController::class,'create']); //untuk mengarahkan ke form input category
Route::post('/category',[CategoriesController::class,'store']);//Menyimpan data category ke DB
Route::get('/category',[CategoriesController::class,'store']);

// Read Data Categories
Route::get('/category', [CategoriesController::class,'index']); // untuk menampilkan semua category ke tabel
Route::get('/category/{id}', [CategoriesController::class,'show']); // untuk menampilkan detail category

// Update Data  Categories
Route::get('/category/{id}/edit', [CategoriesController::class,'edit']); //mengarah ke form edit data
Route::put('/category/{id}',[CategoriesController::class,'update']); //update data berdasarkan id di database

// Delete Data Categories
Route::delete('category/{id}', [CategoriesController::class,'destroy']); // untuk delet data di database table cast berdasarkan ID

// CRUD News
Route::resource('news', NewsController::class);